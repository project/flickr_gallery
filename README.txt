Flickr Gallery module

The purpose of this module is to take everything in Angie Byron's article (http://www.lullabot.com/articles/how_to_build_flickr_in_drupal) at lullabot.com on building Flickr in Drupal and automate it so you don't have to do everything manually.  It is designed for a site that has multiple users uploading photos, and it allows you to search for and categorize by user and taxonomy term.

Required modules
You will need to install the following modules before enabling the Flickr Gallery: module:
* Content (CCK)
* Views
* Taxonomy List
* Imagefield

I also like to use ThickBox for displaying the images, but it is optional.

Next, import the data in the included cck_photo_field.txt as a new CCK type:
copy the data in cck_photo_type.txt
1. go to administer -> content -> content types
2. click on the "Import" tab
3. Paste the copied code into the "Import data" field (make sure Content type is set to "<Create>")
4. Click on "Submit"
This needs to be in place in order for the views created by the module to show up on the Views page.

Installation
Unpackage the .tar to the appropriate modules directory (i.e. mysite/sites/all/modules).

Configuration
1. Go to the Modules page (administer -> site building -> modules), and enable the Flickr Gallery module, as well as the other modules listed above.
2. Go to the user access page (administer -> users -> user access) and enable options for Flickr gallery.
3. Go to Views admin page (administer -> site building -> views) and enable the photos_tags, photos_user, and recent_uploads views.
4. Go to Flick Gallery settings page (administer -> site configuration -> Flickr Gallery settings) and set the options:
* category for categorizing photos - the Flickr Gallery category is created by default
* view for displaying photos by category (photos_tags)
* view for displaying photos by user (photos_user)
* role with permissions to upload photos
5. Go to Site Information page (administer -> site configuration -> site information) and set the front page to mainview (URL for the recent_uploads view).
